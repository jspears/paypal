define(['view'], function (View) {

    return View.extend({
        templateUrl: 'views/transactionHistory.html',
        url: 'rest/transfer',
        formatDate:function(d){
            if (typeof d === 'string'){
                d = new Date(d);
            }
            return [d.getDate(),(1 + d.getMonth()), (1900 + d.getYear())].join('-')
        },
        className:'container'
    });
});