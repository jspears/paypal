define(['view', 'dom', 'xhr', 'app'], function (View, dom, xhr, app) {
    var moneyRe = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;

    return View.extend({
        templateUrl: 'views/sendMoney.html',
        tagName:'form',
        className:'container',
        data: {},
        events: {
            'submit': function (e) {
                e.preventDefault();
                var value = dom.form(e.srcElement)
                console.log('submit', e.srcElement.action, value);
                if (this.isValid()) {
                    console.log('valid')
                    this.onSave(value)
                } else {
                    var el = this.el.querySelector('.invalid input');
                    el && el.focus();
                }
            },
            'change input[type="email"]': function (e) {
                var se = e.srcElement;
                if (se.value && se.value.length > 3) {
                    dom.toggle(se.parentNode, 'invalid', 'valid');
                } else {
                    dom.toggle(se.parentNode, 'valid', 'invalid');
                    se.focus();
                }
                this.isValid();
            },
            'change input[name="amount"]': function (e) {
                var se = e.srcElement;
                if (se.value && se.value.length > 0 && moneyRe.test(se.value)) {
                    dom.toggle(se.parentNode, 'invalid', 'valid');
                } else {
                    dom.toggle(se.parentNode, 'valid', 'invalid');
                    se.focus();
                }
                this.isValid();
            },
            'change select':function(e){
                var se = e.srcElement;
                var o = se.options[se.selectedIndex];
                se.parentNode.setAttribute('data-c', o.text);
            }
        },
        onSave: function (obj) {
            var onFailed = this.onFailed.bind(this);
            xhr.post('rest/transfer', obj).then(function (res) {
                console.log('sucess ', res);
                if (!res || res.error)
                    return onFailed(res.error);
                app.route('views/moneySent.html', res.payload)
            }, function () {
                console.log('failed', arguments);
            });
        },
        onFailed: function (errors) {
            errors = errors ?  errors.errors : errors || {};
            if (!errors) return;
            for(var i in errors){

                var e = dom.one(this.el, '[for="'+i+'"]'), err = errors[i];
                if (!e) return;
                e.title = err.message;
                e.parentNode.classList.add('invalid');
                console.log('err', e, err);
            }
            this.isValid();
        },
        isValid: function () {
            if (!this.el.querySelectorAll('.invalid').length) {
                dom.toggle(this.el, 'invalidated', 'validate');
                return true;
            }
            dom.toggle(this.el, 'validate', 'invalidated');

            return false;
        }
    });

});