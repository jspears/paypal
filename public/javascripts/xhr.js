/**
 * Copyright Justin Spears © - 2014
 *
 * This file sets up routing and rendering for the app.
 *
 * Its a really don't care license, but figured I'll put my name on it.
 * See MIT
 */
define(['promise'], function (promise) {
    promise = promise.promise;
    function createXhr() {
        try {
            return new XMLHttpRequest();
        } catch (e) {
            return new ActiveXObject("Msxml2.XMLHTTP");
        }
    }

    var noResponseType;

    /**
     * Ajax to promise adapter.
     * @param cfg
     * @returns {*}
     */
    function ajax(cfg) {

        var xhr = createXhr(), p = promise(),
            url = cfg.url,
            method = cfg.method || 'GET',
            data = cfg.data && JSON.stringify(cfg.data) + "" || null,
            contentType = cfg.contentType || 'application/json; charset=utf-8',
            responseType = cfg.responseType || 'json';

        if (responseType)
            if (!noResponseType) {
                try {
                    xhr.responseType = responseType;
                } catch (e) {
                    noResponseType = true;
                    xhr = createXhr();
                    console.log("could not set ", responseType);
                }
            }
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    p.resolve(noResponseType && responseType === 'json' ? JSON.parse(xhr.responseText) : xhr.response, xhr);
                } else {
                    p.fail(xhr.statusText, xhr);
                }
            }
        }
        xhr.open(method, url);
        if (data) {
            xhr.setRequestHeader('Content-Type', contentType);
        }
        xhr.send(data);
        return p;
    }

    /**
     * Expose for mocking and overriding.
     * @type {{xhr: ajax, get: get, put: put}}
     */
    var api = {
        parsers: {
            json: function (xhr) {
                if (xhr.response) return xhr.response;
                return JSON.parse(xhr.responseText);
            }
        },
        xhr: ajax,
        get: function (conf) {
            if (typeof conf === 'string') {
                conf = { url: conf}
            }
            return api.xhr(conf);
        },
        post: function (url, data) {
            return api.xhr({
                url: url,
                data: data,
                method: 'POST'
            })
        },
        put: function (url, data) {
            return api.xhr({
                url: url,
                data: data,
                method: 'POST'
            })
        }
    }
    return api;
});