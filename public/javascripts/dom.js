/**
 * Copyright Justin Spears © - 2014
 *
 * This file sets up routing and rendering for the app.
 *
 * Its a really don't care license, but figured I'll put my name on it.
 * See MIT
 */
define(function () {
    /**
     * pulls out the hash or from a window
     * @param url {optional}
     * @returns {string}
     */
    var hash = function (url) {
        url = url || window.location.hash;
        return url.replace(/^#/, '');
    }
    //idea
    //lazy jquery
    var slice = Function.call.bind(Array.prototype.slice);
    var lj = function () {
        var ctx = [];

        function wrap(method, f) {
            return function () {
                ctx.push([
                    {method: method, arguments: slice(arguments, 1) }
                ]);
            }
        }

        this.each = wrap('each');
        this.append = wrap('append');
        this.remove = wrap('append');
        this.replaceWith = wrap('replaceWith')
        this.resolve = function(){

        }
    }


    /**
     * Utility to for doing selection on dom events.
     * @param event - click, hover, etc.
     * @param selector - a.className
     * @param func - callback to fire w
     * @param node - scope of document to query on
     * @return function --- returns the wrapped function to be used with remove
     */
    function listen(event, selector, func, node) {
        node = querySelectorAll(node);

        var listen$event = function (e) {
            var target = e.target;
            if (selector === target) {
                return func.call(target, e);
            }
            var nl = node.querySelectorAll(selector);
            for (var i = 0, l = nl.length; i < l; i++) {
                if (nl[i] !== target) continue;
                func.call(target, e);
                break;
            }
        };
        node.addEventListener(event, listen$event);
        return listen$event;
    }

    function querySelector(node) {
        if (!node) return document;
        if (node.querySelector) return node;
        return querySelector(node.parentNode);
    }

    function querySelectorAll(node) {
        if (!node) return document;
        if (node.querySelectorAll) return node;
        return querySelectorAll(node.parentNode);
    }

    /**
     *
     * @param newChild
     * @param oldChild
     * @return newChild;
     */
    function replace(newChild, oldChild) {
        oldChild.parentNode.replaceChild(newChild, oldChild);
    }

    function _attr_(ele, key, value) {
        if (arguments.length === 2) {
            return ele.getAttribute(key) || '';
        }
        ele.setAttribute(key, value);
        return ele;
    }

    function toObj(form) {
        form = ele(form);
        var ret = {};
        if (!form) return ret;
        //old school dom0 access.
        var inputs = form.elements, i = 0, l = inputs.length;
        for (; i < l; i++) {
            var input = inputs[i], name = input.name, type = (input.tagName === 'INPUT' ? _attr_(input, 'type').toUpperCase() : input.tagName), value;
            if (!name || input.disabled)
                continue;
            console.log(type)
            switch (type) {
                case 'SELECT':
                {

                    if (input.selectedIndex == -1)
                        return null;

                    ret[name] = input.options[input.selectedIndex].text;
                    break;
                }

                case 'RADIO':
                case 'CHECKBOX':
                    if (!input.checked) break;
                case 'TEXTAREA':
                case 'TEXT':
                case 'EMAIL':

                default:
                {
                    ret[name] = input.value;
                }
            }
        }
        return ret;
    }

    function ele(el, select) {
        if (arguments.length === 1) {
            select = el;
            el = document;
        }
        if (typeof select !== 'string')
            return select;
        var nl = querySelector(el).querySelector(select);
        return nl && nl.length ? nl[0] : nl;
    }

    function toggle(el, oldCls, newCls) {
        el = ele(el);
        el.classList.remove(oldCls);
        el.classList.add(newCls);
        return el;
    }

    var api = {
        toggle: toggle,
        one: ele,
        form: toObj,
        hash: hash,
        on: listen,
        replace: replace
    };
    return api;
});