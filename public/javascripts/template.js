/**
 * Copyright Justin Spears © - 2014
 */
// Borrowed with much respect from.
//     Underscore.js 1.6.0
//     http://underscorejs.org
//     (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

define(['xhr', 'promise'], function (xhr, promise) {

    var promise = promise.promise, noMatch = /(.)^/, escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g, escapes = {
        "'": "'",
        '\\': '\\',
        '\r': 'r',
        '\n': 'n',
        '\t': 't',
        '\u2028': 'u2028',
        '\u2029': 'u2029'
    };
    var api = {
        templateSettings: {
            evaluate: /<%([\s\S]+?)%>/g,
            interpolate: /<%=([\s\S]+?)%>/g,
            escape: /<%-([\s\S]+?)%>/g
        },
        pattern: /{{(.*)}}/g,
        compile: function (text, data, settings) {
            //borrowed from John Resig;
            var render;
            settings = settings || api.templateSettings;

            // Combine delimiters into one regular expression via alternation.
            var matcher = new RegExp([
                (settings.escape || noMatch).source,
                (settings.interpolate || noMatch).source,
                (settings.evaluate || noMatch).source
            ].join('|') + '|$', 'g');

            // Compile the template source, escaping string literals appropriately.
            var index = 0;
            var source = "__p+='";
            text.replace(matcher, function (match, escape, interpolate, evaluate, offset) {
                source += text.slice(index, offset)
                    .replace(escaper, function (match) {
                        return '\\' + escapes[match];
                    });

//                if (escape) {
                //                   source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
                //              }
                if (interpolate) {
                    source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
                }
                if (evaluate) {
                    source += "';\n" + evaluate + "\n__p+='";
                }
                index = offset + match.length;
                return match;
            });
            source += "';\n";

            // If a variable is not specified, place data values in local scope.
            if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

            source = "var __t,__p='',__j=Array.prototype.join," +
                "print=function(){__p+=__j.call(arguments,'');};\n" +
                source + "return __p;\n";

            try {
                render = new Function(settings.variable || 'obj', '_', source);
            } catch (e) {
                e.source = source;
                throw e;
            }

            if (data) return render(data, _);
            var template = function (data) {
                return render.call(this, data);
            };

            // Provide the compiled function source as a convenience for precompilation.
            template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

            return template;

        },
        fetch: function (req, data) {
            var p = promise();
            if (typeof req === 'string') {
                req = {
                    url: req,
                    responseType: 'text'
                }
            }
            xhr.get(req).then(function (text) {
                p.resolve(api.compile(text, data));
            }, p.fail);

            return p;
        }
    };

    return api;
});