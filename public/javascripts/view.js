/**
 * Copyright Justin Spears © - 2014
 *
 * This file sets up routing and rendering for the app.
 *
 * Its a really don't care license, but figured I'll put my name on it.
 * See MIT
 */
define(['dom', 'template', 'xhr', 'promise'], function (dom, tmpl, xhr, p) {
    var slice = Function.call.bind(Array.prototype.slice), has = Function.call.bind(Object.hasOwnProperty), promise = p.promise;

    function each(obj, func, scope) {
        if (!obj) return;
        var keys = Object.keys(obj), i = 0, l = keys.length;
        for (; i < l; i++) {
            var key = keys[i];
            if (has(obj, key)) {
                if (func.call(scope, obj[key], key) === false) {
                    return;
                }
            }
        }
    }

    function uneach(obj, func, scope) {
        if (!obj) return;
        var keys = Object.keys(obj), i = 0, l = keys.length;
        for (; i < l; i++) {
            var key = keys[i];
            if (has(obj, key)) {
                if (func.call(scope, key, obj[key]) === false) {
                    return;
                }
            }
        }
    }

    function extend(obj) {
        slice(arguments, 1).forEach(function (source) {
            if (source) {
                for (var prop in source) {
                    obj[prop] = source[prop];
                }
            }
        });
        return obj;
    }

    function isS(str) {
        return typeof str === 'string';
    }

    var View = function View$Constructor(opts) {
        extend(this, opts);
        if (isS(this.el)) {
            this.el = document.querySelector(this.el)[0];
        }
        var el = this.el;

        if (!el) {
            el = this.el = document.createElement(this.tagName || 'div');
        }
        if (this.className) {
            el.classList.add(this.className);
        }
        uneach(this.attributes, el.setAttribute, el);
        this.init(opts);
        this._events = [];
        uneach(this.events, this.onEvent, this);
    }

    extend(View.prototype, {
        init: function View$init() {
        },
        render: function View$render() {
            if (isS(this.template)) this.template = t.compile(this.template);
            if (this.data) {
                this.el.innerHTML = this.template(this);
            }
            this.nodes && each(this.nodes, this.attachNode);
            return this;
        },
        asyncRender: function View$asyncRender() {
            var p = promise();
            var fetchTemplate = function View$asyncRender$fetchTemplate(resp) {
                this.data = resp;
                var self = this;
                if (!this.template && this.templateUrl) {
                    tmpl.fetch(this.templateUrl).then(function (template) {
                        self.template = template;
                        p.resolve(self.render());
                    }, p.fail);
                } else {
                    p.resolve(this.render());
                }
            };
            if (!this.data || this.url) {
                xhr.get(this.url).then(fetchTemplate.bind(this));
            } else {
                fetchTemplate.call(this, this.data);
            }
            return p;
        },
        attachNode: function View$attachNode(key, ref) {
            if (ref[0] !== '$') {
                ref = '$' + ref;
            }
            this[ref] = this.el.querySelector(key);
        },
        onEvent: function View$onEvent(select, handler) {
            var parts = select.split(/\s+/, 2), event = parts.shift(), select = parts.shift() || this.el;
            if (typeof handler === 'string') {
                //in case the property does not exist yet;
                handler = this[handler] || function () {
                    return this[handler].apply(this, slice(arguments));
                }
            }
            this._events.push([event, dom.on(event, select || '*', handler.bind(this), this.el), this.el]);
        },

        offEvent: function View$offEvent(event, handler, node) {
            if (Array.isArray(event)) {
                return this.offEvent.apply(this, event);
            }
            (node || this.el).removeEventListener(event, handler);
        },
        destroy: function View$destroy() {
            this._events.forEach(this.offEvent, this);
            if (this.el && this.el.parentNode) {
                this.el.parentNode.removeChild(this.el);
            }
            return this;
        }

    });
    /**
     * Extend at the prototype or clz level.
     * @param proto
     * @param clz
     * @returns {proto.constructor|*}
     * @constructor
     */
    View.extend = function View$extend(proto, clz) {
        //this will become the super class.
        var spr = this;
        var view$child = proto && has(proto, 'constructor') ? proto.constructor : function () {
            return spr.apply(this, arguments);
        };
        view$child._super_ = this.prototype;

        extend(view$child, this, clz);
        //keep prototype chain
        var RefConstructor = function () {
            this.constructor = view$child;
        };
        RefConstructor.prototype = this.prototype;
        view$child.prototype = new RefConstructor;

        extend(view$child.prototype, proto);
        return view$child;
    };

    return View;
})
;
