/**
 * Copyright Justin Spears © - 2014
 *
 * Its a really don't care license, but figured I'll put my name on it.
 */
(function (win) {
    var api = {}, reUrl = api.reUrl = /\/+/g, has = api.has = Function.call.bind(Object.hasOwnProperty),
        slice = api.slice = Function.call.bind(Array.prototype.slice),
        concat = api.concat = Function.apply.bind(Array.prototype.concat),
        map = api.map = Function.call.bind(Array.prototype.map), scopes = {}, _id = 0, idMap = {}, head = document.getElementsByTagName('head')[0]
        , _currentScript, hostParts = api.hostParts = [window.location.protocol, window.location.host];

    function id() {
        return '__id' + (_id++);
    }


    var conf = win.squire || (window.squire = { });
    if (!has(conf, 'baseUrl')) {
        conf.baseUrl = (function () {
            var href = currentScript();
            console.log('consider setting window.squire.baseUrl', href);
            return href;
        })();
    }
    if (!has(conf, 'paths')) conf.paths = {};
    /**
     * Remove the host and normalize the url, by default returns the document.currentScript.
     * @param url
     * @returns {string}
     */
    function currentScript(url) {
        url = url || ( document.currentScript && document.currentScript.src) || conf.baseUrl;
        var full = url.split(reUrl);
        full.pop();
        return   full.splice(2).join('/');
    }

    function relUrl(url) {
        var full = url.split(reUrl);
        var lastChar = url[url.length - 1];
        if (!(full[0] === api.hostParts[0] && full[1] == api.hostParts[1])) {
            if (!(lastChar === '.' || lastChar === '')) {
                full.pop();
            }
            var path = full.splice(2).join('/'), host = full.join('//');

            return  [host, path].join('/');
        }
        if (!(lastChar === '.' || lastChar === '')) {
            full.pop();
        }
        full.splice(0, 2);
        return full.join('/');
    }

    /**
     * helper function please ignore.
     * @param url
     * @returns {*}
     * @private
     */
    function _normalize_array_(url) {
        return url ? typeof url === 'string' ? url.split(reUrl) : url : void(0);
    }

    /**
     * concat a bunch of urls together normalizing for '//', '.', '..'
     * @param url
     * @returns {Array}
     */
    function normalize(url) {
        var parts = concat([], map(arguments, _normalize_array_)), copy = [], i = 0, l = parts.length;

        for (; i < l; i++) {
            //ignore /./, and undefined and null and empty, zeros are ok.
            if (parts[i] === '.' || parts[i] === '' || parts[i] == null) continue;
            //backup '..'
            if (parts[i] === '..') {
                copy.pop();
                continue;
            }
            copy.push(parts[i]);
        }
        return copy;
    }

    function script(inc) {
        var src = document.createElement('script');
        src.setAttribute('src', inc.src + '.js');
        src.setAttribute('id', inc.id);
        head.appendChild(src);
        return inc;
    }

    function relativise(url) {
        if (url == null || url[0] === '/') return url;
        var urlParts = url.split(reUrl), first = urlParts[0];
        //check it is a local url, if not just return it.
        //this is relative

        if (first === '.' || first === '..') {
            var src = document.currentScript.src;
            var parts = src.split(reUrl);
            if (!(parts[0] === hostParts[0] && parts[1] == hostParts[1])) {
                return url;
            }

            //remove the host
            parts.splice(0, 2);
            //remove the last if it is empty or if it is a file name.
            parts.pop();

            url = normalize(parts, urlParts);
        } else if (has(conf.paths, first)) {
            //replace first part with configuration.
            urlParts.shift();
            url = normalize(conf.baseUrl, conf.paths[first], urlParts);
        } else {
            //otherwise its just baseUrl with url.
            url = normalize(conf.baseUrl, url);
        }
        return url.join('/');
    }

    /**
     * resolves a module.
     * @param modules
     * @param callback
     */
    function resolve(modules) {
        modules = modules || [];
        var p = promise();

        promise().all(modules.map(function (v, i) {
            //if it is not relative we can check the map, to see if its been defined.
            if (v[0] !== '.') {
                if (has(scopes, v)) {
                    //it may be possible to have a scope and not be a module, so that should fall through.
                    return scopes[v].module;
                }
            }
            var loaderParts = v.split('!', 2);
            v = loaderParts.pop();

            var src = relativise(v);
            console.log('resolving ', src);
            var scope = (scopes[src] || scopes[v]);
            if (scope && has(scope, 'module'))
                return scope.module;
            var _id = id();
            scope = scope || (scopes[src] = idMap[_id] = { id: _id, src: src, promise: promise() });
            scope.promise.then(function (mod) {
                scope.module = mod;
            });
            var ret = plugin(loaderParts, scope);
            return ret.module || ret.promise;
        })).then(function (arg) {
            var args = to_args(arg);
            p.resolve(args);
        });
        return p;
    }

    function plugin(loader, scope) {
        var p = promise();
        if (loader.length === 0) {
            loader.push('/__default_loader__');
        }

        p.then(function (args) {
            args[0].load(scope);
        })
        resolve(loader).then(p.resolve, p.fail);
        return scope;

    }


    /**
     *
     * @param paths - variatic
     * @param callback
     * @param error;
     */
    function require(modules, callback, error) {
        return resolve(modules).then(function (args) {
            return callback && callback.apply(null, args);
        }, error);

    }


    function to_args(arg) {
        var args = [];
        if (typeof arg === 'undefined') {
            return args;
        }
        for (var i = 0, l = arg.length; i < l; i++) {
            args.push(arg[i][0]);
        }
        return args;

    }

    /**
     * RequireJS/AMD define.
     * @param [name]
     * @param [modules]...
     * @param callback
     */
    function define(name, modules, callback) {
        var args = slice(arguments);
        callback = args.pop();
        if (args.length <= 1) {
            if (Array.isArray(name)) {
                modules = name;
                name = null;
            } else {
                modules = [];
            }
        }
        if (name === callback) name = null;
        if (name && modules.length === 0) {
            var ret;
            if (typeof callback === 'function') {
                ret = callback();
            } else {
                ret = callback;
            }

            return promise().resolve(( scopes[name] = {
                module: ret,
                id: id()
            }).module);
        }
        var _id = document.currentScript && document.currentScript.getAttribute('id');
        return resolve(modules).then(function (arg) {

            var scope = modById(_id), result;
            if (typeof callback === 'function') {
                result = callback.apply(this, arg);
            } else {
                result = callback;
            }
            if (name) scopes[name] = { module: result, id: _id }

            if (scope && has(scope, 'promise')) {
                scope.promise.resolve(result);
                delete scope.promise;
            }

        });
    }

    function modById(id) {
        var ref = idMap[id];
        //prevent a leak
        delete idMap[id];
        return ref;
    }

    function promise_error() {
        throw new Error("Promise already complete");
    }

    function sort(a, b) {
        return a.i - b.i;
    }

    function extract(a) {
        return a.value;
    }

    function noop() {
    }

    function Promise(success, error) {
        var all = [], done = noop, toRef;
        if (success || error) {
            all.push([success, error]);
        }
        var f = this;
        f.timeout = function (timeout) {
            if (timeout) {
                toRef = setTimeout(f.fail, timeout, new Error("Timeout exceeded [" + timeout + "ms]"));
            }
            return f;
        }
        /**
         * Take an array or var args of promises and waits until all complete.
         * @returns {Promise}
         */
        f.all = function () {
            var complete = [], a = concat([], arguments);
            var hasError = false;

            /**
             * Keeps order and fires the resolve.
             * @param values
             * @param errors
             * @param pos
             * @returns {boolean}
             */
            function check(values, errors, pos) {
                var ret = false;
                if (values != null || (hasError = errors != null)) {
                    complete.push({value: values, errors: errors, i: pos});
                    ret = true;
                }
                if (complete.length === a.length) {
                    var c = complete.sort(sort);
                    if (hasError) {
                        f.fail(c.filter(error).map(extract), complete.map(extract));
                    } else {
                        f.resolve(complete.map(extract));
                    }
                }
                return ret;
            }

            a.forEach(function Promise$all(p, i) {
                if (p instanceof Promise) {
                    if (!check(p._values, p._errors, i)) {
                        p.then(function Promise$all_success(val) {
                            check(slice(arguments), null, i);
                        }, function Promise$all_error() {
                            check(null, slice(arguments), i);
                        });
                    }
                } else {
                    check([p], null, i);
                }
            });

            setTimeout(check, 0);
            return f;
        };

        f.then = function (success, error) {
            if (!(success == null && error == null)) {
                all.push([success, error]);
            }
            return f;
        };

        f.resolve = function (value) {
            clearTimeout(toRef);
            var args = slice(arguments);
            f._values = args;
            var err = Promise.promiseError.bind(f, args);
            all.forEach(function Promise$resolve$forEach(v, i) {
                if (v[0]) {
                    v[0].apply(this, args);
                    v[0] = err;
                }
            }, f);
            done();
            return f;
        };
        f.fail = function (value) {
            clearTimeout(toRef);
            var args = this._errors = slice(arguments);
            var err = Promise.promiseError.bind(f, args);
            all.forEach(function (v, i) {
                if (v[1]) {
                    v[1].apply(this, args);
                    v[1] = err;
                }
            }, f);
            done();
            return f;
        };
        f.done = function (cb) {
            if (cb)
                done = cb;
        }
    }

    Promise.promiseError = promise_error

    function promise(success, error) {
        return new Promise(success, error);
    }

    define('/__default_loader__', {
        load: script
    });
    define('promise', {
        promise: promise,
        Promise: Promise
    });
    define('squire', api);

    api.relativise = relativise;

    api.normalize = normalize;

    api.relUrl = relUrl;

    if (!has(win, 'define')) {
        win.define = define;
    }
    if (!has(win, 'require')) {
        win.require = require;
    }


})(window)