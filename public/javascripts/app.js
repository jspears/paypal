/**
 * Copyright Justin Spears © - 2014
 *
 * This file sets up routing and rendering for the app.
 *
 * Its a really don't care license, but figured I'll put my name on it.
 * See MIT
 */
define([ 'dom'], function (dom) {

    var api = {currentView: false}, to;

    var render = api.route = function (url, data) {
        api.busy(true);
        var body = dom.one(api.contentEl);
        url = (url || api.defaultView).replace(/^#/, '');

        var isView = /\.html$/.test(url);
        /**
         * If it ends end .html then use the generic view with the view as the
         * template. Otherwise use the included
         */
        return require([isView ? 'view' : url], function (View) {
            var view = new View(isView ? {
                templateUrl: url,
                data: data || {data: data}
            } : {});
            view.asyncRender().then(function () {
                clearTimeout(to);
                if (api.currentView) {
                    var el = api.currentView.el;
                    dom.replace(view.el, el);
                    api.currentView.destroy();
                    api.currentView = view;

                } else {
                    api.currentView = view;
                    body.appendChild(view.el);
                }
                api.busy(false);
            })
        });
    }
    api.busy = function api$busy(busy) {
        var classList = dom.one(api.contentEl).classList;

        clearTimeout(to);
        if (busy) {
            classList.add('busy')
            to = setTimeout(api.busy, api.loadDelay);
        } else {
            classList.remove('busy');
            clearTimeout(to);
        }
        return to;
    }
    //listen to hash changes
    api.start = function () {
        render(dom.hash());
        var hash = window.location.hash;
        setInterval(function () {
            if (hash !== window.location.hash) {
                hash = window.location.hash;
                console.log('hash', hash);
                render(dom.hash());
            }
        }, 150)
    }

    return api;

})