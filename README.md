#PayPal Sample App
###Justin M Spears [mailto:speajus@gmail.com](speajus@gmail.com);
--------

#Design
So I wanted to try and build a relatively modern webapp without any
3rd party libraries.   So I got started, and very soon wanted AMD/requirejs,
but as I stated I wanted to try it without 3rd parties libs,
so I wrote my own.   To implement a proper AMD,  I needed proper Promises,
so I wrote that too [see squire.js](./public/javascripts/squire.js). 


#CSS
So I went with flex layout, since this is going to be only tested on Chrome.  I am
a little newer to it than I should be, and haven't gotten full control of it yet.  Otherwise
I am using CSS3 for transitions,   and UTF-8 icons for checkbox's and so forth.


##Views
Views are dynamically created, and determined by the window.location.hash.
If the view ends in .html, than a default view [see view.js](./public/javascripts/view.js) 
object is created and the view template that matches will be rendered [see app.js](./public/javascripts/app.js).  
If it does not end .html than it assumes what is being required is the view and just
requires it. This allows for the smallest possible initial load, while still programming
like a regular (non-SPA) web application.   To tie  it all together I needed a template 
system, I borrowed heavily from underscore,  [see template.js](./public/javascripts/template.js),
random dom utils [see dom.js](./public/javascripts/dom.js), an [xhr](./public/javascripts/xhr.js)
wrapper.


#Testing
While I didn't want to use 3rd party tools for the implementation, I had no such hesitation
for testing. I am using Karma + mocha for testing.  This was really easy to setup, and have
used to great effect in the past.

```bash
$ npm install
$ karma start
```

#Server Side
Server side is a default [expressjs](http://expressjs.com/) app, with a project of mine called 
[mers](https://github.com/jspears/mers) for exposing mongoose objects as rest objects.  I haven't
used this project in a while but it still worked, swimmingly.



#Installation
After getting the source and starting mongod.

```bash
$ npm install
$ ./bin/www
$ open  [http://localhost:3000](http://localhost:3000)
```

#Bugs/Todo
* CSS is a little wonky, mostly because I wrote static pages then, I made an app out of it
some of the css where inconsistent.  Given a couple of good minutes I can fix, but this has
been a bit of a time sync.

* Add more tests especially to failure routes of squire.js.  

* Validation, validation, validation.


