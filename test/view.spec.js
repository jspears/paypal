describe('view', function () {
    this.timeout(0)
    before(function () {
        var conf = window.squire || (window.squire = {});
        conf.baseUrl = 'base/public/javascripts'
    });

    it('view should be required', function (done) {
        require(['view'], function (View) {

            expect(View).to.not.be.null;

            done();

        }, done);
    })
    it('view should be async renderable', function (done) {
        require(['view'], function (View) {

            var P = View.extend({
                templateUrl: 'base/test/support/paypal.html',
                url: 'base/test/support/data.json'
            });
            var p = new P();
            p.asyncRender().done(function () {
                console.log(p.el.innerHTML);
                document.body.appendChild(p.el);
                done();
            });

        }, done);
    })


})