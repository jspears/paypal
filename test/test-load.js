define(['./support/rel/b'], function (b) {
    return {
        load: function (scope) {
            scope.promise.resolve({
                hello: 'world',
                src: scope.src,
                b: b
            });
            return scope;
        }
    };
});