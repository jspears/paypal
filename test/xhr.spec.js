describe('xhr spec', function () {
    before(function(){
        var conf = window.squire || (window.squire = {});
        conf.baseUrl = 'base';
        conf.paths['javascripts'] = 'public/javascripts';
    });

    it('should make a get of json', function (done) {

        require(['javascripts/xhr'], function (x) {
            x.get('base/test/support/data.json').then(function (r) {
                expect(r).to.be.not.null;
                expect(r.name).to.eq('paypal');
                done();
            })
        })
    });
    it('should make a get of json and abort', function (done) {
        require(['javascripts/xhr'], function (x) {
            x.get('base/test/support/data.jsonjunk').then(null, function (r) {
                expect(r).to.be.not.null;
                done();
            })
        })
    });
});