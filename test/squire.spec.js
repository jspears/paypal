describe("squire test suite", function () {
    this.timeout(0);
    var baseHostUrl = window.location.protocol + '//' + window.location.host, promise;
    before(function (done) {
        var conf = window.squire || (window.squire = {});
        conf.baseUrl = 'base';
        conf.paths = {
            'support': 'test/support'
        }
        require(['promise'], function (p) {
            promise = p.promise;
            done();
        })
    })
    describe('squire plugins', function () {
        it('should require a plugin', function () {
            require(['test/test-load!stuff'], function (scope) {
                expect(scope.src).to.be.eq('base/stuff');
            });
        })

    });

    describe('require impl', function () {

        it('should have require', function () {
            expect(require).to.be.defined;
        });
        it('should have define', function () {
            expect(define).to.be.defined;
        });
        it('should require m.js', function (test) {
            require(['support/m'], function (m) {
                expect(m.m).to.eq(1);
                test();
            });
        });
        it('should require a.js', function (test) {
            require(['support/a'], function (a) {
                expect(a.m.m).to.eq(1);
                test();
            });
        });
        it('should require support/rel/b', function (test) {
            require(['support/rel/b'], function (b) {
                expect(b.b.m.m).to.eq(1);
                expect(b.m).to.eq(b.b.m);
                test();
            });

        })

    })

    describe('promise implementation', function () {

        it('should make a promise', function () {
            var val;
            var p = promise(function (v) {
                val = v;
            })
            p.resolve(1);
            expect(val).to.eq(1);

        });
        it('should make a promise with setTimeout', function (done) {
            var p = promise(function (v) {
                expect(v).to.eq(3);

            }).then(function () {
                done()
            });
            setTimeout(p.resolve, 200, 3);

        });
        it('should satisfy all promise with setTimeout', function (done) {
            var p = promise(function (v) {
                expect(v).to.eq(3);

            }).then(function () {
                done()
            });
            setTimeout(p.resolve, 200, 3);

        });
        it('should satisfy all promises', function (done) {
            var p1 = promise(), p2 = promise(), p3 = promise(), p4 = promise();
            //all ready resolved check;
            p4.resolve(5);
            promise(function (v) {
                expect(v[0][0]).to.eq(0);
                expect(v[1][0]).to.eq(1);
                expect(v[2][0]).to.eq(2);
                expect(v[3][0]).to.eq(4);
                expect(v[4][0]).to.eq(5);
            }).all(p1, p2, p3, 4, p4).done(done);

            //resolve out of order
            setTimeout(p1.resolve, 200, 0);
            setTimeout(p2.resolve, 100, 1);
            setTimeout(p3.resolve, 300, 2);

        });
        it('should timeout if one is set', function (done) {

            var p = promise(function (v) {
                expect(v).to.eq(3);

            }).timeout(100).then(null, function () {
                done()
            });
            setTimeout(p.resolve, 200, 3);

        });
        it('all should take an array', function (done) {
            var p1 = promise(), p2 = promise(), p3 = promise(), p4 = promise();
            //all ready resolved check;
            p4.resolve(5);
            promise(function (v) {
                expect(v[0][0]).to.eq(0);
                expect(v[1][0]).to.eq(1);
                expect(v[2][0]).to.eq(2);
                expect(v[3][0]).to.eq(4);
                expect(v[4][0]).to.eq(5);
            }).all([p1, p2, p3, 4, p4]).done(done);

            //resolve out of order
            setTimeout(p1.resolve, 200, 0);
            setTimeout(p2.resolve, 100, 1);
            setTimeout(p3.resolve, 300, 2);

        });
    });
});