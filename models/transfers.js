module.exports = function (m) {
    var mongoose = m, Schema = m.Schema;
    var currency = ['USD', 'EUR', 'JPY'], paymentFor = ['friends', 'goods'];
    var Transfers = new Schema({
        emailTo: {
            type: String,
            required: true
        },
        message: {
            type: String

        },
        paymentFor: {
            type: String,
            enum: paymentFor,
            required: true
        },
        amount: {
            type: Number,
            min: 1,
            required: true
        },
        timestamp: {
            type: Date,
            default: Date.now
        },
        currency: {
            type: String,
            enum: currency
        }
    });
    var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
    //TODO - add charecter restrictions.
        size = /^(.){0,254}$/g;
    Transfers.path('emailTo').validate(function (email) {
        return emailRegex.test(email); // Assuming email has a text attribute
    }, 'The e-mail field cannot be empty.');

    Transfers.path('message').validate(function (msg) {
        if (!(msg || msg.length)) return true;
        return msg.length < 255; // Assuming email has a text attribute
    }, 'The message field has a max length of 254 charecters.');

    return m.model('transfer', Transfers);

};

